export default {
    baseUrl: 'http://localhost:8080/springboot9fcw0/',
    indexNav: [
        {
            name: '首页',
            url: '/index/home'
        },
        {
            name: '志愿组织',
            url: '/index/zhiyuanzuzhi'
        },
        {
            name: '组织风采',
            url: '/index/zuzhifengcai'
        },
        {
            name: '志愿活动',
            url: '/index/zhiyuanhuodong'
        },
        {
            name: '公告信息',
            url: '/index/news'
        },
        {
            name: '留言板',
            url: '/index/messages'
        },
    ]
}
